## Easy web form creation

### Best web forms can be created with hi-style themes

Looking for web form creation? No where to go. We are here to help you out on this by our fabulous web form service

#### Our features:
* A/B Testing

* Form Conversion
* Form Optimization
* Branch logic
* Payment integration
* Third party integration
* Push notifications
* Multiple language support
* Conditional logic
* Validation rules
* Server rules
* Custom reports

### We can create forms in a visual way similar to creating menus in the backend

It’s easy to add new fields, and reorder fields by drag and drop our [web form creator](https://formtitan.com). It produces shortcodes so you can insert forms anywhere in your site.

Happy web form creation!